import { injectGlobal, css } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  *:focus {
    outline: none;
  }
`;


export const mainTeme = {
  navBackground: '#000',
  navText: '#8e8e8e',
  navTextActive: '#fff',
  navTextHover: '#fd5454',
  ctaBackground: '#fd5454',
  ctaColor: '#fff',
  ctaBackgroundHover: '#ff7676',
};


const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 376,
};

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label]}px) {
      ${css(...args)}
    }
  `;

  return acc;
}, {});
