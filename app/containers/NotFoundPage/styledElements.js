import styled from 'styled-components';

import { ContactFormContainer } from '../../components/ContactForm/styledElements';

export const NotFoundPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  .notfound404 {
    flex-grow: 1;
  }
`;

export const NotFoundContainer = ContactFormContainer.extend`
  a .fa {
      color: #eee;
      &:hover {
          color: ${(props) => props.theme.navTextHover};
      }
  }
`;
