/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Helmet } from 'react-helmet';
import { NavLink } from 'react-router-dom';
import Navigation from 'components/Navigation';
import Footer from 'components/Footer';
import { Notification } from 'components/ContactForm/styledElements';

import messages from './messages';

import { NotFoundPageContainer, NotFoundContainer } from './styledElements';

export default class NotFound extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <NotFoundPageContainer>
        <Helmet>
          <title>Not found</title>
          <meta name="description" content="Not found" />
        </Helmet>
        <Navigation></Navigation>
        <NotFoundContainer className="notfound404">
          <h1>
            <FormattedMessage {...messages.header} />
          </h1>
          <Notification>
            <div className="icon"><i className="fa fa-exclamation" aria-hidden="true"></i></div>
            <h2>
              <FormattedMessage {...messages.continue} />
              <NavLink to="/">
                <i className="fa fa-home" aria-hidden="true"></i>
              </NavLink>
            </h2>
          </Notification>
        </NotFoundContainer>
        <Footer></Footer>
      </NotFoundPageContainer>
    );
  }
}
