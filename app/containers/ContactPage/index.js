/**
 *
 * ContactPage
 *
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import Navigation from 'components/Navigation';
import ContactForm from 'components/ContactForm';
import Footer from 'components/Footer';
import { ContactPageContainer } from './styledElements';

export default class ContactPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ContactPageContainer>
        <Helmet>
          <title>Contact</title>
          <meta name="description" content="Contact page" />
        </Helmet>
        <Navigation />
        <ContactForm className="contact-form" />
        <Footer />
      </ContactPageContainer>
    );
  }
}
