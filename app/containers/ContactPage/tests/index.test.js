import React from 'react';
import { shallow } from 'enzyme';

import { ContactPage } from '../index';

describe('<ContactPage />', () => {
  it('should render the page message', () => {
    const renderedComponent = shallow(
      <ContactPage />
    );
    expect(renderedComponent.contains(
      <title> </title>
    )).toEqual(true);
  });
});
