import styled from 'styled-components';

export const ContactPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;

  .contact-form {
    flex-grow: 1;
  }
`;
