/**
*
* InstagramFeed
*
*/

import React from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './messages';
import { getFeed } from './getFeed';
import { InstagramFeedContainer, PhotosCollection, FeedItem } from './styledElements';

class InstagramFeed extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      feed: null,
    };
  }

  componentDidMount() {
    getFeed()
      .then((feed) => this.setState({ feed }));
  }

  render() {
    const { feed } = this.state;

    return (
      <InstagramFeedContainer>
        <h2><FormattedMessage {...messages.header} /></h2>
        <h3><a href="//instagram.com/tamtamnl" target="_blank"><FormattedMessage {...messages.handle} /></a></h3>
        <PhotosCollection>
          { feed
            ? feed.map((item) => (
              <FeedItem key={item.id}>
                <img src={item.image} alt={item.text} />
                <div className="text">
                  <p>
                    {item.text}
                  </p>
                </div>
              </FeedItem>
            ))
            : <i className="fa fa-spinner fa-spin fa-3x fa-fw" />
          }
        </PhotosCollection>
      </InstagramFeedContainer>
    );
  }
}

InstagramFeed.propTypes = {

};

export default InstagramFeed;
