import React from 'react';
import { shallow } from 'enzyme';

import InstagramFeed from '../index';

describe('<InstagramFeed />', () => {
  it('should render instagram feed in a styledElements__InstagramFeedContainer wrapper', () => {
    const wrapper = shallow((
      <InstagramFeed>
      </InstagramFeed>
    ));
    expect(wrapper.find('styledElements__InstagramFeedContainer').node).toBeDefined();
  });
});
