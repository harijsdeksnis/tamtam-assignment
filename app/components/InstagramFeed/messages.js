/*
 * InstagramFeed Messages
 *
 * This contains all the text for the InstagramFeed component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.InstagramFeed.header',
    defaultMessage: ' ',
  },
  handle: {
    id: 'app.components.InstagramFeed.handle',
    defaultMessage: ' ',
  },
});
