import { range } from 'lodash';

const feedCaptions = [
  'Last Thursday emerce organised their own ADE and we were part of it! Where are you gonna party tonight? #ADE#tbt #enight #tamtam',
  'We\'re feeling happier and happier thanks to these nice dutch flowers we received from Florensis! #holland #flowers #doordebloemenofficenietmeerzien #settingthebloemetjesoutside',
  'We are VERY VERY happy with our Silver Lovie Award and People\'s Lovies Winner for the Docters without borders website! #lovies #tamtamhome #proud #celebration',
  'Thanks @radio538 and @maxpinas for bringing us friday beats with our brand new #marshall speakers! #friyay #gift #speaker #tamtamhome',
  'Friyay! Coffee anyone? #almostweekend #tamtamhome #koffiemax #latteart',
  'TamTam Talks Search Edition! First edition with our new colleagues from Expand Online👌💪🏻 #tamtamhome #tttalks #breakfast #search',
];

const feed = range(1, 7).map((item) => ({
  id: item,
  image: require(`../../images/feed/${item}.jpg`), // eslint-disable-line global-require
  text: feedCaptions[item - 1],
}));

export const getFeed = () => new Promise((resolve) => {
  // setTimeout(() => resolve(feed), 2000);
  resolve(feed);
});
