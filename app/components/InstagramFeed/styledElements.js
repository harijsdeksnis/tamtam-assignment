import styled from 'styled-components';
import { media } from '../../global-styles';

const feedBg = require('../../images/feed/background.jpg'); // eslint-disable-line global-require

export const InstagramFeedContainer = styled.section`
  min-height: 800px;
  text-align: center;
  background-image: url(${feedBg});
  background-size: cover;
  background-position: center bottom;
  padding-top: 80px;
  padding-bottom: 180px;
  padding-left: 0;
  padding-right: 0;

  h2 {
    font-size: 30px;
    font-weight: 900;
    text-transform: uppercase;
    color: #789199;
    line-height: 40px;
    margin: 0 0 20px 0;
  }
  h3 {
    font-size: 30px;
    line-height: 40px;
    font-weight: 300;
    margin: 0;
    a {
      color: ${(props) => props.theme.navTextActive};
      text-decoration: none;
      &:hover {
        color: ${(props) => props.theme.navTextHover};
      }
    }
  }

  ${media.phone`
    padding-top: 55px;
    padding-bottom: 90px;
    h2 {
      font-size: 20px;
      line-height: 26px;
      margin-bottom: 10px;
    }
    h3 {
      font-size: 20px;
      line-height: 30px;
    }
  `}

`;

export const PhotosCollection = styled.div`
  margin-top: 40px;
  display: inline-flex;
  flex-wrap: wrap;
  justify-content: center;
  text-align: center;
  max-width: 960px;

  ${media.phone`
    margin-top: 30px;
  `}
`;

export const FeedItem = styled.div`
  display: block;
  width: 300px;
  margin: 10px;

  img {
    width: 300px;
  }

  .text {
    background: #fff;
    padding: 15px;
    p {
      font-family: 'Lato', sans-serif;
      font-size: 15px;
      line-height: 21px;
      color: #76797f;
    }
  }
`;
