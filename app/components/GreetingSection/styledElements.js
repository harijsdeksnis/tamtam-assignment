import styled from 'styled-components';

import { media } from '../../global-styles';

export const GreetingSectionContainer = styled.section`
  background: #fff;
  color: #000;
  text-align: center;
  font-family: 'Lato', sans-serif;
  padding: 80px 20px;

  h2 {
    font-size: 30px;
    font-weight: 900;
    text-transform: uppercase;
    color: #000;
    margin-top: 0;
    margin-bottom: 20px;
  }

  p {
    display: inline-block;
    max-width: 940px;
    font-family: 'Lato', sans-serif;
    font-size: 24px;
    line-height: 34px;
    font-weight: 300;

    ${media.tablet`
      font-size: 20px;
      line-height: 28px;
    `}
  }

  ${media.phone`
    padding: 60px 10px;

    h2 {
      font-size: 20px;
      line-height: 28px;
      margin-bottom: 15px;
    }
    p {
      font-size: 15px;
      line-height: 20px;
    }
  `}

`;
