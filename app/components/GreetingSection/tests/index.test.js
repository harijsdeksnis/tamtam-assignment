import React from 'react';
import { shallow } from 'enzyme';

import GreetingSection from '../index';

describe('<GreetingSection />', () => {
  it('should render greetings section in a styledElements__GreetingSectionContainer wrapper', () => {
    const wrapper = shallow((
      <GreetingSection>
      </GreetingSection>
    ));
    expect(wrapper.find('styledElements__GreetingSectionContainer').node).toBeDefined();
  });
});
