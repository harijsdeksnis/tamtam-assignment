/*
 * GreetingSection Messages
 *
 * This contains all the text for the GreetingSection component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.GreetingSection.header',
    defaultMessage: ' ',
  },
  body: {
    id: 'app.components.GreetingSection.body',
    defaultMessage: ' ',
  },
});
