/**
*
* GreetingSection
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { GreetingSectionContainer } from './styledElements';
import messages from './messages';

class GreetingSection extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <GreetingSectionContainer id="greeting">
        <h2><FormattedMessage {...messages.header} /></h2>
        <p>
          <FormattedMessage {...messages.body} />
        </p>
      </GreetingSectionContainer>
    );
  }
}

GreetingSection.propTypes = {
  heading: PropTypes.string,
  text: PropTypes.string,
};

export default GreetingSection;
