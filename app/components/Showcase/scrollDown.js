export const scrollDown = () => {
  const contentStart = document.getElementById('greeting');
  if (contentStart) {
    const { y } = contentStart.getBoundingClientRect();
    doScrolling(y, 1000);
  }
};

// https://stackoverflow.com/questions/17722497/scroll-smoothly-to-specific-element-on-page
const doScrolling = (elementY, duration) => {
  const startingY = window.pageYOffset;
  const diff = elementY - startingY;
  let start;

  // Bootstrap our animation - it will get called right before next frame shall be rendered.
  window.requestAnimationFrame(function step(timestamp) {
    if (!start) start = timestamp;
    // Elapsed miliseconds since start of scrolling.
    const time = timestamp - start;
    const t = time / duration;
    const percent = Math.min(t * (2 - t), 1);

    window.scrollTo(0, (diff * percent) + startingY);
    // Proceed with animation as long as we wanted it to.
    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  });
};

