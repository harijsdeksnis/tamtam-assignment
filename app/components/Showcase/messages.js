/*
 * Showcase Messages
 *
 * This contains all the text for the Showcase component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  viewCase: {
    id: 'app.components.Showcase.viewCase',
    defaultMessage: ' ',
  },
});
