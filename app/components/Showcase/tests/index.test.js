import React from 'react';
import { shallow } from 'enzyme';

import Showcase from '../index';

describe('<Showcase />', () => {
  it('should render showcase in a styledElements__ShowcaseContainer wrapper', () => {
    const wrapper = shallow((
      <Showcase>
      </Showcase>
    ));
    expect(wrapper.find('styledElements__ShowcaseContainer').node).toBeDefined();
  });
});
