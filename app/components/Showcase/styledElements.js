import styled from 'styled-components';
import { media } from '../../global-styles';

const walibiBg = require('../../images/carousel/Walibi.jpg'); // eslint-disable-line global-require
const mouseImg = require('../../images/mouse.png'); // eslint-disable-line global-require
const mouseImgHover = require('../../images/mouse-hover.png'); // eslint-disable-line global-require
const arrowDownTouch = require('../../images/arrow-down-touch.png'); // eslint-disable-line global-require
const arrowDownTouchActive = require('../../images/arrow-down-touch-active.png'); // eslint-disable-line global-require

export const ShowcaseContainer = styled.section`
  background: ${(props) => props.theme.navTextActive};
  height: 700px;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  background: #000;

  ${media.phone`
    height: 508px;
  `}
`;

export const ShowcaseSlide = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-image: url(${(props) => props.slideBg});
  background-size: cover;
  background-position: center bottom;
  opacity: 0;
  transform: scale(1.05);
  will-change: opacity;
  transition-duration: .5s;

  &.active {
    opacity: 1;
    transform: scale(1);
  }
`;

ShowcaseSlide.defaultProps = {
  slideBg: walibiBg,
};

export const ShowcaseControls = styled.div`
  position: relative;
  z-index: 1000;
  margin-top: 279px;

  h2 {
    color: ${(props) => props.theme.navBackground};
    text-transform: uppercase;
    text-align: center;
    font-size: 50px;
    line-height: 60px;
    margin-top: 0;
    margin-bottom: 40px;

  }

  ${media.tablet`
    margin-top: 290px;
    h2 {
      font-size: 40px;
      line-height: 48px;
      margin-bottom: 30px;
    }
  `}

  ${media.phone`
    margin-top: 167px;
    h2 {
      font-size: 30px;
      line-height: 36px;
    }
  `}
`;

export const ViewCase = styled.button`
  display: inline-block;
  border-radius: 20px;
  background: ${(props) => props.theme.navBackground};
  color: ${(props) => props.theme.navTextActive};
  width: 150px;
  height: 40px;
  font-size: 15px;
  line-height: 38px;
  font-weight: 400;
  text-align: center;
  margin:0 20px;
`;

export const SliderArrow = styled.button`
  display: inline-block;
  border-radius: 100%;
  background: ${(props) => props.theme.navBackground};
  color: ${(props) => props.theme.navTextActive};
  width: 40px;
  height: 40px;
  text-align: center;
  line-height: 40px;
  &:hover {
    cursor: pointer;
    background: ${(props) => props.theme.navTextHover};
  }
`;

export const ScrollButton = styled.div`
  display: inline-block;
  position: relative;
  z-index: 1000;
  margin-top: auto;
  margin-bottom: 15px;
  width: 54px;
  text-align: center;

  button {
    border-radius: 100%;
    background-color: #fff;
    background-image: url(${mouseImg});
    background-position: center center;
    width: 54px;
    height: 54px;
    &:hover {
      cursor: pointer;
      background-image: url(${mouseImgHover});
    }

  }
  i.fa {
    color: #fff;
  }

  ${media.tablet`
    margin-bottom: 20px;

    button, button:hover {
      width: 40px;
      height: 40px;
      background-image: url(${arrowDownTouch});
      &:active {
        background-image: url(${arrowDownTouchActive});
      }
    }
    i.fa {
      display: none;
    }
  `}
`;
