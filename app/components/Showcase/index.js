/**
*
* Showcase
*
*/

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { ShowcaseContainer, ShowcaseSlide, ShowcaseControls, ViewCase, SliderArrow, ScrollButton } from './styledElements';
import { scrollDown } from './scrollDown';

const slides = ['Florensis', 'Oxxio', 'Walibi'].reduce((acc, item, idx) => {
  acc[idx] = {
    id: idx,
    name: item,
    src: require(`../../images/carousel/${item}.jpg`), // eslint-disable-line global-require
  };
  return acc;
}, []);

class Showcase extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
    };
  }

  nextSlide = () => {
    let { activeSlide } = this.state;
    activeSlide += 1;
    if (activeSlide >= slides.length) {
      activeSlide = 0;
    }
    this.setState({ activeSlide });
  }

  prevSlide = () => {
    let { activeSlide } = this.state;
    activeSlide -= 1;
    if (activeSlide < 0) {
      activeSlide = slides.length - 1;
    }
    this.setState({ activeSlide });
  }

  scrollDown = () => scrollDown();

  render() {
    const { activeSlide } = this.state;

    return (
      <ShowcaseContainer>
        <ShowcaseControls>
          <h2>{slides[activeSlide].name}</h2>
          <SliderArrow onClick={this.prevSlide}>
            <i className="fa fa-chevron-left"></i>
          </SliderArrow>
          <ViewCase>
            <FormattedMessage {...messages.viewCase} />
          </ViewCase>
          <SliderArrow onClick={this.nextSlide}>
            <i className="fa fa-chevron-right"></i>
          </SliderArrow>
        </ShowcaseControls>
        <ScrollButton onClick={this.scrollDown}>
          <button> &nbsp; </button>
          <i className="fa fa-chevron-down"></i>
        </ScrollButton>
        {slides.map((slide) =>
          <ShowcaseSlide className={slide.id === activeSlide ? 'active' : ''} slideBg={slides[activeSlide].src} key={slide.id} />
        )}
      </ShowcaseContainer>
    );
  }
}

export default Showcase;
