import { get, find } from 'lodash';

export const inputFields = [{
  id: 'firstName',
  type: 'text',
  placeholder: 'First name',
  validation: {
    required: 'We need your first name.',
  },
},
{
  id: 'lastName',
  type: 'text',
  placeholder: 'Last name',
  validation: {
    required: 'We need your last name.',
  },
},
{
  id: 'email',
  type: 'email',
  placeholder: 'Your e-mail address',
  validation: {
    required: 'We need your email',
    email: 'Please use a valid e-mail address.',
  },
},
{
  id: 'phone',
  type: 'text',
  placeholder: 'Your phone number (optional)',
},
];

const validationFields = inputFields.concat({
  id: 'message',
  validation: {
    required: 'Sorry, your message can’t be empty.',
  },
});

const validateEmail = (email) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(email).toLowerCase()); // eslint-disable-line no-useless-escape

export const getValidationMessage = (id) =>
  (validationKey) => get(find(validationFields, { id }), ['validation', validationKey], 'Sorry, this field is not valid.');


export const validateField = (id, value) => {
  const validations = Object.keys(get(find(validationFields, { id }), 'validation', {}));
  let fieldValidity = true;

  validations.forEach((check) => {
    switch (check) {
      case 'required':
        if (value === '') fieldValidity = check;
        break;
      case 'email':
        if (!validateEmail(value)) fieldValidity = check;
        break;
      default:
        break;
    }
  });

  return fieldValidity;
};
