/**
*
* ContactForm
*
*/

import React from 'react';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { ContactFormContainer, ContactFormFields, InputContainer, TextareaContainer, Send, ValidationMessage, Notification } from './styledElements';

import { inputFields, getValidationMessage, validateField } from './inputFields';

class ContactForm extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      message: '',
      showErrors: false,
      showSuccess: false,
      sending: false,
    };
  }

  fieldValidity = {
    firstName: 'required',
    lastName: 'required',
    email: 'required',
    message: 'required',
  };

  handleFieldChange = (field) => (event) => {
    this.setState({ [field]: event.target.value });
    this.fieldValidity[field] = validateField(field, event.target.value);
  };

  handleFormSubmit = () => {
    const canSubmit = Object.keys(this.fieldValidity).every((field) => this.fieldValidity[field] === true);

    if (!canSubmit) {
      this.setState({ showErrors: true });
    } else {
      this.setState({ sending: true });
      // simulate async backend call in UI
      setTimeout(() => this.setState({ sending: false, showSuccess: true }), 2000);
    }
  };

  render() {
    const { ...props } = this.props;
    const { ...state } = this.state;

    return (
      <ContactFormContainer {...props}>

        <h1><FormattedMessage {...messages.header} /></h1>

        {state.showSuccess
          ? <Notification>
            <div className="icon"><i className="fa fa-check" aria-hidden="true"></i></div>
            <h2><FormattedMessage {...messages.success} /></h2>
          </Notification>
          : <div>
            {state.showErrors
              ? <Notification>
                <div className="icon"><i className="fa fa-exclamation" aria-hidden="true"></i></div>
                <h2><FormattedMessage {...messages.error} /></h2>
              </Notification>
              : null
            }
            <ContactFormFields>
              {inputFields.map((field) => (
                <InputContainer key={field.id}>
                  <input
                    className={field.id}
                    type={field.type}
                    placeholder={field.placeholder}
                    value={state[field.id]}
                    onChange={this.handleFieldChange(field.id)}
                  />
                  {this.fieldValidity[field.id] === true && field.validation
                    ? <i className="fa fa-check" aria-hidden="true"></i>
                    : null
                  }
                  {state.showErrors && field.validation && this.fieldValidity[field.id] !== true
                    ? <ValidationMessage>{getValidationMessage(field.id)(this.fieldValidity[field.id])}</ValidationMessage>
                    : null
                  }
                </InputContainer>))
              }

              <TextareaContainer>
                <textarea placeholder="Your message…" value={state.message} onChange={this.handleFieldChange('message')}></textarea>
                {this.fieldValidity.message === true
                  ? <i className="fa fa-check" aria-hidden="true"></i>
                  : null
                }
                {state.showErrors && this.fieldValidity.message !== true
                  ? <ValidationMessage>{getValidationMessage('message')(this.fieldValidity.message)}</ValidationMessage>
                  : null
                }
              </TextareaContainer>
            </ContactFormFields>
            <Send onClick={this.handleFormSubmit} disabled={state.sending}>
              {state.sending
                ? <span><FormattedMessage {...messages.pleaseWait} /> <i className="fa fa-spinner fa-spin" /></span>
                : <FormattedMessage {...messages.send} />
              }
            </Send>
          </div>
        }

      </ContactFormContainer>
    );
  }
}

ContactForm.propTypes = {

};

export default ContactForm;
