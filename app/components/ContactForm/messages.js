/*
 * ContactForm Messages
 *
 * This contains all the text for the ContactForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ContactForm.header',
    defaultMessage: ' ',
  },
  send: {
    id: 'app.components.ContactForm.send',
    defaultMessage: ' ',
  },
  pleaseWait: {
    id: 'app.components.ContactForm.pleaseWait',
    defaultMessage: ' ',
  },
  success: {
    id: 'app.components.ContactForm.success',
    defaultMessage: ' ',
  },
  error: {
    id: 'app.components.ContactForm.error',
    defaultMessage: ' ',
  },
});
