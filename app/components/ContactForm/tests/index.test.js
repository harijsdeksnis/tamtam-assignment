import React from 'react';
import { shallow } from 'enzyme';

import ContactForm from '../index';

describe('<ContactForm />', () => {
  it('should render contact from in a styledElements__ContactForm wrapper', () => {
    const wrapper = shallow((
      <ContactForm>
      </ContactForm>
    ));
    expect(wrapper.find('styledElements__ContactForm').node).toBeDefined();
  });
});
