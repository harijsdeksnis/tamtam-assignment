import styled from 'styled-components';
import { media } from '../../global-styles';

const feedBg = require('../../images/feed/background.jpg'); // eslint-disable-line global-require

export const ContactFormContainer = styled.section`
  text-align: center;
  background-image: url(${feedBg});
  background-size: cover;
  background-position: center bottom;
  background-attachment: fixed;
  padding-top: 100px;
  padding-bottom: 200px;
  padding-left: 0;
  padding-right: 0;
  text-align: center;

  h1 {
    margin-left: auto;
    margin-right: auto;
    margin-top: 0;
    margin-bottom: 40px;
    max-width: 380px;
    color: #fff;
    text-transform: uppercase;
    font-size: 36px;
    line-height: 50px;
    text-align: center;
  }
  ${media.tablet`
    padding-top: 80px;
    h1 {
      font-size: 26px;
      line-height: 36px;
    }
  `}
  ${media.phone`
    padding-top: 20px;
    padding-bottom: 100px;
    h1 {
    max-width: 260px;
      font-size: 20px;
      line-height: 28px;
      margin-bottom: 20px;
    }
  `}
`;

export const ContactFormFields = styled.form`
  display: inline-flex;
  flex-wrap: wrap;
  justify-content: space-between;
  width: 100%;
  max-width: 620px;
  padding: 20px 10px;
  background: rgba(255, 255, 255, .2);
  ${media.phone`
    justify-content: center;
  `}
`;

export const InputContainer = styled.div`
  width: 100%;
  max-width: 295px;
  margin-bottom: 10px;
  position: relative;

  input, textarea {
    display: block;
    width: 100%;
    height: 50px;
    border: 0;
    background: #fff;
    color: #000;
    font-size: 18px;
    line-height: 50px;
    font-weight: 300;
    padding: 0 50px 0 20px;
    &.phone {
      padding-right: 20px;
    }
  }

  .fa.fa-check {
    position: absolute;
    right: 15px;
    top: 17px;
    color: #7BDB4C;
    font-size: 19px;
  }
  @media (max-width: 608px) {
    max-width: 600px;
  }
  ${media.phone`
    width: 300px;
  `}
`;

export const TextareaContainer = InputContainer.extend`
  width: 100%;
  max-width: 600px;
  margin-bottom: auto;
  margin-top: 10px;
  textarea {
    width: 100%;
    height: 120px;
    padding: 10px 20px;
    line-height: 25px;
  }
  ${media.phone`
    width: 300px;
  `}
`;

export const ValidationMessage = styled.div`
  width: 100%;
  height: 40px;
  background: #555D60;
  padding: 0 20px;
  line-height: 38px;
  font-size: 15px;
  font-weight: 300;
  color: #fff;
  position: relative;
  text-align: left;

  &:before {
    content: " ";
    position: absolute;
    left: 25px;
    top: -6px;
    width: 11px;
    height: 11px;
    background: #555D60;
    display: inline-block;
    transform: rotate(45deg);
  }
`;

export const Send = styled.button`
  display: block;
  border-radius: 20px;
  background: ${(props) => props.theme.ctaBackground};
  color: ${(props) => props.theme.ctaColor};
  width: 150px;
  height: 40px;
  font-size: 15px;
  line-height: 40px;
  font-weight: 400;
  text-align: center;
  margin: 40px auto;

  &:hover {
    cursor: pointer;
    background: ${(props) => props.theme.ctaBackgroundHover};
  }
  &[disabled] {
    opacity: 0.6;
  }
`;

export const Notification = styled.div`
  width: 100%;
  max-width: 620px;
  height: 60px;
  background: rgba(0, 0, 0, .2);
  margin:0 auto;
  text-align: left;
  div.icon {
    display: inline-block;
    width: 30px;
    height:30px;
    border-radius: 100%;
    margin: 15px 20px 15px 15px;
    background: rgba(0, 0, 0, .2);
    color: #fff;
    text-align: center;
    font-size: 18px;
    line-height: 30px;
  }
  h2 {
    display: inline-block;
    color: #fff;
    font-size: 18px;
    line-height: 60px;
    font-weight: 400;
    margin: 0;
  }
  ${media.phone`
    height: 80px;
    div.icon {
      position: relative;
      top: -10px;
      margin: 15px 20px 25px 25px;
    }
    h2 {
      padding-top: 15px;
      line-height: 25px;
      max-width: 200px;
    }
  `}
`;
