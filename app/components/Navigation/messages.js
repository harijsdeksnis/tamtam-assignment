/*
 * Navigation Messages
 *
 * This contains all the text for the Navigation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'app.components.Navigation.home',
    defaultMessage: ' ',
  },
  people: {
    id: 'app.components.Navigation.people',
    defaultMessage: ' ',
  },
  contact: {
    id: 'app.components.Navigation.contact',
    defaultMessage: ' ',
  },
});
