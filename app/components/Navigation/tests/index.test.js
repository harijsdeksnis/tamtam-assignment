import React from 'react';
import { shallow } from 'enzyme';

import Navigation from '../index';

describe('<Navigation />', () => {
  it('should render navigation in a styledElements__NavigationContainer wrapper', () => {
    const wrapper = shallow((
      <Navigation>
      </Navigation>
    ));
    expect(wrapper.find('styledElements__NavigationContainer').node).toBeDefined();
  });
});
