/**
*
* Navigation
*
*/

import React from 'react';
import { NavLink } from 'react-router-dom';

import { FormattedMessage } from 'react-intl';
import { NavigationContainer, NavItem, NavItemLinks, CloseButton } from './styledElements';

import messages from './messages';
const logoImage = require('../../images/logo.png'); // eslint-disable-line global-require

class Navigation extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      mobileMenuOpen: false,
    };
  }

  toggleMobileMenu = () => this.setState((prevState) => ({ mobileMenuOpen: !prevState.mobileMenuOpen }));

  render() {
    const { mobileMenuOpen } = this.state;

    return (
      <NavigationContainer>
        {mobileMenuOpen
          ? <CloseButton onClick={this.toggleMobileMenu}></CloseButton>
          : <i className="fa fa-bars" aria-hidden="true" onClick={this.toggleMobileMenu}></i>
        }
        <NavItem className="logo">
          <img src={logoImage} alt="Tam Tam" />
        </NavItem>
        <NavItemLinks className={mobileMenuOpen ? 'open' : ''}>
          <NavItem>
            <NavLink activeClassName="selected" exact to="/">
              <FormattedMessage {...messages.home} />
            </NavLink>
          </NavItem>
          <NavItem>
            <FormattedMessage {...messages.people} />
          </NavItem>
          <NavItem>
            <NavLink activeClassName="selected" to="/contact">
              <FormattedMessage {...messages.contact} />
            </NavLink>
          </NavItem>
        </NavItemLinks>
      </NavigationContainer>
    );
  }
}

Navigation.propTypes = {

};

export default Navigation;
