import styled from 'styled-components';

import { media } from '../../global-styles';
const closeBtnBg = require('../../images/close.png'); // eslint-disable-line global-require


export const NavigationContainer = styled.section`
  height: 80px;
  background: ${(props) => props.theme.navBackground};
  padding-left: 30px;

  .fa.fa-bars {
    display: none;
  }

  ${media.phone`
    padding-left: 20px;
    height: 60px;

    .logo {
      text-align: right;
      margin-right: 0;
      width: 42vw;
      transform: scale(.8) translateY(-15px);
    }

    .fa.fa-bars {
      display: inline-block;
      color: #fff;
      font-size: 35px;
      line-height: 60px;
      transform: translateY(-5px);
      width: 30px;
    }
  `}
`;

export const NavItem = styled.div`

  display: inline-block;
  color: ${(props) => props.theme.navText};
  line-height: 80px;
  font-size: 18px;
  font-weight: 400;
  margin-right: 40px;

  a {
    color: ${(props) => props.theme.navText};
    text-decoration: none;
    &.selected {
      color: ${(props) => props.theme.navTextActive};
    }
    &:hover {
      color: ${(props) => props.theme.navTextHover};
    }
  }
  &:last-child {
    margin-right: 0;
  }


`;

export const NavItemLinks = styled.div`
  display: inline-block;

  ${media.phone`
    display: none;
    background: ${(props) => props.theme.navBackground};
    border-top: 2px solid ${(props) => props.theme.navText};
    position: absolute;
    z-index: 1010;

    &.open {
      display: block;
      top: 60px;
      left: 0;
      right: 0;
      bottom: 0;
      padding-top: 30px;
      & > div {
        display: block;
        text-align: center;
        font-size: 30px;
        line-height: 44px;
        font-weight: 900;
        margin: 0;
        height: 90px;
        a: hover {
          color: red;
        }
      }
    }
  `}
`;

export const CloseButton = styled.span`
    display: none;
    width: 30px;
    height: 24px;
    background: url(${closeBtnBg}) no-repeat 0 0;
    transform: translateY(-5px);
    ${media.phone`
      display: inline-block;
    `}
`;
