import React from 'react';
import { shallow } from 'enzyme';

import Footer from '../index';

describe('<Footer />', () => {
  it('should render footer in a styledElements__FooterContainer wrapper', () => {
    const wrapper = shallow((
      <Footer>
      </Footer>
    ));
    expect(wrapper.find('styledElements__FooterContainer').node).toBeDefined();
  });
});
