import styled from 'styled-components';

export const FooterContainer = styled.section`
  display: flex;
  height: 100px;
  background: ${(props) => props.theme.navBackground};
  text-align: center;
  color: ${(props) => props.theme.navTextActive};
  line-height: 100px;
  font-size: 20px;

  div.social {
    width: calc(100% - 140px);
    text-align: center;

    a {
      display: inline-block;
      color: ${(props) => props.theme.navTextActive};
      margin:0 20px;
      &:hover {
        color: ${(props) => props.theme.navTextHover};
      }
    }
  }
`;

export const LanguageSwitch = styled.div`
  display: block;
  width: 70px;

  button {
    font-size: 15px;
    display: inline-block;
    color: #ddd;
    text-decoration: none;
    &:hover {
      cursor: pointer;
      color: ${(props) => props.theme.navTextHover};
    }
  }
`;
