/**
*
* Footer
*
*/

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { FooterContainer, LanguageSwitch } from './styledElements';
import { changeLocale } from '../../containers/LanguageProvider/actions';

export class Footer extends React.Component { // eslint-disable-line react/prefer-stateless-function

  setLocale = (locale) => () => {
    const { dispatch } = this.props;
    dispatch(changeLocale(locale));
  };

  render() {
    return (
      <FooterContainer>
        <LanguageSwitch>
          <button onClick={this.setLocale('en')}>EN</button>
          <button onClick={this.setLocale('nl')}>NL</button>
        </LanguageSwitch>
        <div className="social">
          <a href="//www.facebook.com/tamtamnl/" target="_blank"><i className="fa fa-facebook"></i></a>
          <a href="//www.twitter.com/tamtamnl" target="_blank"><i className="fa fa-twitter"></i></a>
          <a href="//instagram.com/tamtamnl" target="_blank"><i className="fa fa-instagram"></i></a>
        </div>
      </FooterContainer>
    );
  }
}

Footer.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Footer);
