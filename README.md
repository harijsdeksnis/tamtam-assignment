# Tam tam assignment

## Using it
    - Clone the repo
    - install with npm or yarn
    - run `npm run start` for dev server
    - or `npm run build` and serve the /app/build directory

## Write-up

Since I come mainly from Angular background and some Vue lately, prior this assignment, I had only contributed to already set up React projects.
I had added/modified some relatively simple components, passing props, setting state etc.
Some experience with routing and I had added some 3rd party library components to existing projects.

So this is the first React project I'm setting up from scratch.
I chose https://github.com/react-boilerplate/react-boilerplate as a start setup because it seems well valued in the community with 17k stars.
It's a bit of an overkill for this assignment app, but for me it's a great way to jump into the ecosystem and learn about the tools/libraries in use.

At first I felt a little bit over my head, because before getting to coding, I had to refresh my React knowledge,
read up on boilerplate documentation how it is intended to be used,
and try to understand and read docs of used libraries (styled-components, react-intl, react-redux etc...)
I know those concepts well from Angular ecosystem, but how it's supposed to be used is slightly different between implementations.

Some time got spent experimenting how this all works and few wrong turns were made in the beginning.
But after a while I got hang of it and started being productive.

One thing I'm really happy that I discovered is `styled-components` library that allows a very easy html element creation with styles attched to them.
Adding media queries never has been easier..

The `react-intl` also seems like a valuable addition to get multi-language support. Also a great new library I found out and learned about during this assignment.

I explored ideas to use some libary for the form field validation and carousel. After losing some time trying to configure them and making them
work/appear as specified in designs, I decided not to loose more time and just implement that myself.


I approached the assignment in a MVP fashion; I added bare minimum to meet most features reasonably well to save time and be effective.
With the idea that later, when I have time for that, I'll iterate further and improve.
For example, some shortcuts I took

    - I used font awesome icons where possible, to save time with exporting images and writing custom classes with them as background
    - I implemented Instagram feed first with static images and captions. It is because the old Instagram API has recently been depreciated and
    Graph API is getting introduced. So client_id is no longer enough to get feed. You also need access token, which requries oAuth flow.
    I was afraid I might lose too much time on that and left that for later. And it doesn't seem like intended UX for this app - that a user should
    go through oAuth flow to get to see instagram feed. Perhaps this requirement in the assignment is outdated (due to Instagram API policy change) or I am not getting something?
    - I used flexbox for instagram feed layout, although it looks like it needs some masonry library to be exactly as in the design.
    - I developed and tested using Chrome. I tried not to lose time testing on other browsers. In a real scenario I would use https://www.browserstack.com/
        to make sure my solution works on all intended devices.


Some small extras I did:

    - Simulate API call and show loading state when contact form gets submitted
    - Added a not found page when you navigate to a URL that doesn't exist..
    - I added a simple Language switching feature because the boiler plate comes with that,
        but I did not have enough time to place it beautifully in design and to make all messages translatable (like form placeholders and error messages)
        You can find the language switch in the footer.
        I might have violated a principle of accessing store in a "dumb" component (footer). I was still figuring out the best "right" way to do it
        as the time was running out..


All in all I'm pretty satisfied that I picked up development with React so fast.
I didn't get to using store and react-redux a lot because (1) there was a lot else that kept me busy (2) there isn't too much app state to manage anyways.
However router and language provider uses it, so I got to work with it indirectly.

I spent sime time to get components reasonably "pixel perfect". I'm sure it's not completely ideal everywhere, but to a naked eye it should be very ok.

Best regards,
Harijs